<?php

    // Write a function that generates string of random characters with length that is given as an argument. The function should use this expression:

    // chr(rand(97,122)) ==========> generates random character from a to z


    function generateRandomString($lenght){
        $string = "";

        $leters = range('a','z');

        for ($i=0; $i <$lenght ; $i++) { 
            $string .= $leters[mt_rand(0,(count($leters)-1))];
        }

        return $string;
    }


    echo generateRandomString(mt_rand(97,122));
?>