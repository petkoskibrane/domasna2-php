<?php

    // Write a function to display the given number in reverse order.
    function reverse($number){
        $number = (string) $number;
        
        $reversenumber = "";

        
        for($i=(strlen($number)-1); $i >=0 ; $i--) { 
               
            $reversenumber .= $number[$i];
        }


        return $reversenumber;
    }

    echo reverse(567);

?>
