<?php

// Write a function to display the first n terms of Fibonacci series.


function fibonacciNumbers($number)
{
    $firstnumber =0;
    $secondnumber =1;
    for ($i=1; $i < ($number-1); $i++) {
        if ($i == 1) {
            echo ' '.$firstnumber." ";
        }
        if ($i == 2) {
            echo '  '.$secondnumber;
        }


        $nextnumber = $firstnumber + $secondnumber;

        echo " ".$nextnumber." ";
        $firstnumber = $secondnumber;
        $secondnumber = $nextnumber;
    }
}


    echo fibonacciNumbers(10);
