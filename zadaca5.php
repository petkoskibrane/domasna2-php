<?php

    // Write a function that prints the multiplication table of a given integer.

    function printMultiplicationTable($number){
        
        for ($i=1; $i <=10 ; $i++) { 
            echo $number ." x ".$i." = ".($number*$i)."<br>";
        }

    }

    printMultiplicationTable(5);

?>