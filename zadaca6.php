<?php
        // Write a function that calculates the factorial of a given number.


        function calculateFactorial($number){
            $factorial = $number;
            
            if($number <= 1){
                $factorial = 1;                
            }else{
                
                $factorial = $number * calculateFactorial($number-1);
                 
            }
            return $factorial;
        }

         echo calculateFactorial(5);   
?>